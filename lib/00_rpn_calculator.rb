class RPNCalculator

  attr_accessor :stack

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_calc(:+)
  end

  def minus
    perform_calc(:-)
  end

  def times
    perform_calc(:*)
  end

  def divide
    perform_calc(:/)
  end

  def tokens(str)
    symbols = [:+, :-, :*, :/]
    tokens = str.split
    tokens.map do |token|
      if symbols.include?(token.to_sym)
        token.to_sym
      else
        token.to_i
      end
    end
  end

  def evaluate(str)
    tokenized = tokens(str)

    tokenized.each do |token|
      if token.is_a?(Integer)
        push(token)
      else
        perform_calc(token)
      end
    end
    value
  end

  def value
    @stack.last
  end


  private

  def perform_calc(sym)
    raise "calculator is empty" if @stack.count < 2
    second_num = @stack.pop
    first_num = @stack.pop

    case sym
    when :+
      @stack << first_num + second_num
    when :-
      @stack << first_num - second_num
    when :*
      @stack << first_num * second_num
    when :/
      @stack << first_num.fdiv(second_num)
    end
  end

end
